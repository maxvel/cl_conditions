-module(cl_conditions).

-export([
  init/0,
  handler_bind/2,
  handler_case/2,
  signal/1,
  error/1,
  restart_bind/2,
  restart_case/2,
  find_restart/2,
  invoke_restart/2,
  compute_restarts/0,
  unwind_protect/2
]).

-define(HANDLERS, cl_conditions_handlers).
-define(RESTARTS, cl_conditions_restarts).

-type exception_type() :: atom().
-type exception() :: exception_type() | {exception_type(), any()}.
-type handler() :: fun((exception()) -> any() | no_return()).

-type restart_name() :: atom().
-type restart_param() :: any().
-type restart() :: fun((restart_param()) -> any()).

-type body() :: fun(() -> any()).

-spec init() -> ok.
init() ->
  Self = self(),
  case lists:member(?MODULE, erlang:registered()) of
    true -> ok;
    false ->
      Pid = erlang:spawn_link(fun() ->
        _ = ets:new(?HANDLERS, [named_table, public]),
        _ = ets:new(?RESTARTS, [named_table, public]),
        Self ! done,
        receive _ -> ok
        end
      end),
      erlang:register(?MODULE, Pid),
      receive done -> ok
      end
  end.

-spec handler_bind(body(), #{exception_type() => handler()}) -> any().
handler_bind(Body, HandlersMap) ->
  Handlers = handlers(),
  NewHandlers = maps:to_list(HandlersMap),
  Handlers2 = NewHandlers ++ Handlers,
  set_handlers(Handlers2),
  try Body()
  after
    set_handlers(Handlers)
  end.

-spec handler_case(body(), #{exception_type() => handler()}) -> any().
handler_case(Body, HandlersMap) ->
  Tag = make_ref(),
  CallbacksMap = maps:map(fun(_, Handler) ->
    fun(Exception) -> throw({Tag, Handler(Exception)}) end
  end, HandlersMap),
  try handler_bind(Body, CallbacksMap)
  catch throw:{Tag, Value} -> Value
  end.

-spec signal(exception()) -> any().
signal(Exception) ->
  Handlers = handlers(),
  case lists:search(fun({Type, _}) -> is_instance_of(Type, Exception) end, Handlers) of
    {value, {_, Handler}} -> Handler(Exception);
    false -> ok
  end.

-spec error(exception()) -> none().
error(Exception) ->
  _ = signal(Exception),
  %% TODO this is a place to add debugger
  throw(Exception).

-spec restart_bind(body(), #{restart_name() => restart()}) -> any().
restart_bind(Body, RestartsMap) ->
  Restarts = restarts(),
  NewRestarts = maps:to_list(RestartsMap),
  Restarts2 = NewRestarts ++ Restarts,
  set_restarts(Restarts2),
  try Body()
  after
    set_restarts(Restarts)
  end.

-spec restart_case(body(), #{restart_name() => restart()}) -> any().
restart_case(Body, RestartsMap) ->
  Tag = make_ref(),
  CallbacksMap = maps:map(fun(_, Restart) ->
    fun(Param) -> throw({Tag, Restart(Param)}) end
  end, RestartsMap),
  try restart_bind(Body, CallbacksMap)
  catch throw:{Tag, Value} -> Value
  end.

-spec find_restart(atom(), boolean()) -> restart() | undefined | no_return().
find_restart(Name, ThrowOnError) ->
  Restarts = restarts(),
  case lists:search(fun({RName, _}) -> RName == Name end, Restarts) of
    {value, {_, Restart}} -> Restart;
    false when ThrowOnError -> throw({restart_not_found, Name});
    false -> undefined
  end.

-spec invoke_restart(restart_name(), restart_param()) -> any().
invoke_restart(Name, Param) ->
  Restart = find_restart(Name, true),
  Restart(Param).

-spec compute_restarts() -> [{restart_name(), restart()}].
compute_restarts() ->
  restarts().

-spec unwind_protect(body(), fun()) -> any().
unwind_protect(Body, After) ->
  try Body()
  after After()
  end.

%% Internal

-spec is_instance_of(exception_type(), exception()) -> boolean().
is_instance_of(Type, Type) -> true;
is_instance_of(Type, {Type, _}) -> true;
is_instance_of(_Type, _) -> false.

-spec handlers() -> [{exception_type(), handler()}].
handlers() ->
  case ets:lookup(?HANDLERS, self()) of
    [] -> [];
    [{_, Value}] -> Value
  end.

-spec set_handlers([{exception_type(), handler()}]) -> true.
set_handlers(Value) ->
  ets:insert(?HANDLERS, {self(), Value}).

-spec restarts() -> [{restart_name(), restart()}].
restarts() ->
  case ets:lookup(?RESTARTS, self()) of
    [] -> [];
    [{_, Value}] -> Value
  end.

-spec set_restarts([{restart_name(), restart()}]) -> true.
set_restarts(Value) ->
  ets:insert(?RESTARTS, {self(), Value}).
