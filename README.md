cl_conditions
=====

An OTP library providing Common Lisp-style condition restart system for Erlang.

Common Lisp has a very advanced condition system of which other languages only cover error handling (`try/catch`) part. This condition system allows you to go back and forth between the place where a signal (including unexpected situations) occured and the place which actually can decide what to do using different options presented by the code in the middle.

The detailed description can be found here: http://www.gigamonkeys.com/book/beyond-exception-handling-conditions-and-restarts.html

Build
-----

    $ rebar3 compile

Usage examples
-----

Error handling

Suppose you want to do something with a text data split across a bunch of files. The code might look like this:

```erlang
main() ->
  Parsed = [parse_file(F) || F <- filelib:wildcard("*")],
  %% ... do other staff

parse_file(File) ->
  {ok, Data} = file:read_file(File),
  [parse_line(L) || L <- binary:split(Data, <<"\n">>)].

parse_line(Line) ->
  [Id, Name] = string:split(Line, <<",">>),
  {binary_to_integer(Id), Name}.
```

In this piece of code there are several places where things might go wrong:

* `{ok, Lines} = file:read_file(File)` if file can not be read
* `[Id, Name] = string:split(Line, <<",">>),` if a line contains more (or less) commas
* `{binary_to_integer(Id), Name}.` if `Id` has non-diget charaters in it

Currently in either case the whole thing will fail. To make it not fail we can try to add `try/catch` to these places but it is "builtin" and the calling code has no control over it.

This is how the condition restart system can handle this case:

```erlang
parse_file(File) ->
  Data = cl_conditions:restart_case(
    fun() ->                                              %% original code which can produce an error
      case file:read_file(File) of
        {ok, Data} -> Data;
        _ -> cl_conditions:error({file_not_found, File}) %% we provide error type and some payload
      end
    end
    %% restart name => restart code
    #{ skip_file => fun(_) -> <<>> end }
  ),
  Lines = [parse_line(L) || L <- binary:split(Data, <<"\n">>)],
  [L <- Lines, L /= bad_line].

parse_line(Line) ->
  Split = cl_conditions:restart_case(
    fun() ->
      case string:split(Line, <<",">>) of
        [Id, Name] -> {Id, Name};
        _ -> cl_conditions:error({bad_line, Line})
      end
    end,
    #{ skip_line => fun(_) -> bad_line end }
  ),
  case Split of
    bad_line -> bad_line;
    {Id, Name} ->
      IntId = cl_conditions:restart_case(
        fun() ->
          try binary_to_integer(Id)
          catch badarg -> cl_conditions:error({bad_integer, Id})
          end
        end,
        %% the parameter comes from restart invokator
        #{ replace_value => fun(NewId) -> NewId end }
      ),
      {IntId, Name}
  end.
```

So what we made here is we wrapped every dangerous piece of logic into a `cl_conditions:restart_case` and each provides a workaround. Here's how we can now use it in our `main` function:

```erlang
main() ->
  Parsed = [begin
    cl_conditions:handler_case(
      fun() -> parse_file(F) end, %% code which might produce the signal
      #{
        %% signal type => signal handler
        file_not_found => fun(_) -> cl_conditions:invoke_restart(skip_file, none) end,
        bad_line       => fun(_) -> cl_conditions:invoke_restart(skip_line, none) end,
        bad_integer    => fun(_) -> cl_conditions:invoke_restart(replace_value, -1) end
      }
    )
  end || F <- filelib:wildcard("*")],
  %% ...
```

What we have here is our top level caller now decides which actions to do in case of each type of error as well as using solutions provided by `parse_file` and `parse_line` to skip file or line. For the third case we chose to supply a default `-1` id to records for which `id` could not be parsed instead of ignoring them.


Communicating with the caller

Error handling is just one of the applications of the system. In general it allows code pieces to communicate across the stack, here's an example of a task signalling it's progress:

```erlang
%% generate N numbers satisfying the predicate
gen_numbers(N, Predicate) -> gen_numbers(N, Predicate, []).
gen_numbers(0, _Predicate, Acc) -> Acc;
gen_numbers(N, Predicate, Acc) ->
  X = rand:uniform(1000),
  case Predicate(X) of
    true ->
      cl_conditions:signal(progress, length(Acc)+1),
      gen_numbers(N-1, Predicate, [X | Acc]);
    false -> gen_numbers(N, Predicate, Acc)
  end
```

The calling code can decide to display this as logs or progress bar or just ignore.

```erlang
main() ->
  Numbers = cl_conditions:handler_bind(
    fun() -> gen_numbers(100, fun(X) -> X div 2 == 0 end) end,
    #{ progress => fun(N) -> io:format("~p out of ~p completed\n", [N, 100]) end }
  ),
  ...
```

Internals
-----

Under the hood it uses two ETS tables to store per-process stacks of handlers & restarts. Also builtin `try/catch/after` mechanism is used to jump between execution contexts.

TODO
-----

* [DONE] Allow multiple handlers/restarts without nesting them
* Integrate standard Erlang error types
* Add a debugger?