-module(cl_conditions_SUITE).

-include_lib("common_test/include/ct.hrl").

%% Test server callbacks
-export([
  all/0
]).

-export([
  test_handler_bind/1,
  test_handler_case_uw_protect/1,
  test_restart_case/1,
  test_multiple_handlers/1
]).

all() ->
  [
    test_handler_bind,
    test_handler_case_uw_protect,
    test_restart_case,
    test_multiple_handlers
  ].

test_handler_bind(_Config) ->
  cl_conditions:init(),
  Log = ets:new(tmp, [ordered_set]),
  Ret = cl_conditions:handler_bind(
    fun() ->
      trace(Log, body_begin),
      Ret = div_signal(10, 0),
      trace(Log, body_end),
      Ret
    end,
    #{
      division_by_zero => fun(Ex) -> trace(Log, {handler_call, Ex}) end
    }
  ),
  0 = Ret,
  [
    {_, body_begin},
    {_, {handler_call, division_by_zero}},
    {_, body_end}
  ] = ets:tab2list(Log),
  ok.

test_handler_case_uw_protect(_Config) ->
  cl_conditions:init(),
  Log = ets:new(tmp, [ordered_set]),
  Ret = cl_conditions:handler_case(
    fun() ->
      trace(Log, body_begin),
      cl_conditions:unwind_protect(
        fun() ->
          trace(Log, uw_begin),
          Ret = div_signal(10, 0),
          trace(Log, not_reached),
          Ret
        end,
        fun() -> trace(Log, after_block) end
      )
    end,
    #{ division_by_zero => fun(Ex) -> trace(Log, {handler_call, Ex}), -5 end }
  ),
  -5 = Ret,
  [
    {_, body_begin},
    {_, uw_begin},
    {_, {handler_call, division_by_zero}},
    {_, after_block}
  ] = ets:tab2list(Log),
  ok.

test_restart_case(_Config) ->
  cl_conditions:init(),
  Log = ets:new(tmp, [ordered_set]),
  Ret = cl_conditions:handler_case(
    fun() ->
      cl_conditions:restart_case(
        fun() ->
          trace(Log, body_begin),
          div_error(5, 0),
          trace(Log, not_reached)
        end,
        #{
          return_value => fun(Val) ->
            trace(Log, {restart, Val}),
            Val
          end
        }
      )
    end,
    #{
      division_by_zero => fun(Ex) ->
        trace(Log, {handler_begin, Ex}),
        cl_conditions:invoke_restart(return_value, -1),
        trace(Log, not_reached)
      end
    }
  ),
  -1 = Ret,
  [
    {_, body_begin},
    {_, {handler_begin, division_by_zero}},
    {_, {restart, -1}}
  ] = ets:tab2list(Log),
  ok.

test_multiple_handlers(_Config) ->
  cl_conditions:init(),
  Log = ets:new(tmp, [ordered_set]),
  Ret = cl_conditions:handler_case(
    fun() ->
      trace(Log, body_begin),
      R = multiple_signals(),
      trace(Log, body_end),
      R
    end,
    #{
      first  => fun(Arg) -> trace(Log, {first, Arg}),  cl_conditions:invoke_restart(return, 1) end,
      second => fun(Arg) -> trace(Log, {second, Arg}), cl_conditions:invoke_restart(return, 2) end,
      third  => fun(Arg) -> trace(Log, {third, Arg}),  cl_conditions:invoke_restart(return, 3) end
    }
  ),
  3 = Ret,
  [
    {_, body_begin},
    {_, {first, first}},
    {_, {second, {second, 1}}},
    {_, {third, {third, 2}}},
    {_, body_end}
  ] = ets:tab2list(Log),
  ok.


%% Internal
div_signal(_X, 0) ->
  cl_conditions:signal(division_by_zero),
  0;
div_signal(X, Y) -> X / Y.

div_error(_X, 0) -> cl_conditions:signal(division_by_zero);
div_error(X, Y) -> X / Y.

multiple_signals() ->
  Val1 = cl_conditions:restart_case(
    fun() -> cl_conditions:signal(first) end,
    #{ return => fun(V) -> V end }
  ),
  Val2 = cl_conditions:restart_case(
    fun() -> cl_conditions:signal({second, Val1}) end,
    #{ return => fun(V) -> V end }
  ),
  cl_conditions:restart_case(
    fun() -> cl_conditions:signal({third, Val2}) end,
    #{ return => fun(V) -> V end }
  ).

trace(Into, Msg) ->
  ets:insert(Into, {erlang:monotonic_time(), Msg}).
